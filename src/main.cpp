#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct category
{
        string categoryName;
        int categoryPosition;
        int categoryLayer;
        vector<int> categoryChildren;
        int categoryParent;
        vector<int> categoryTags;
};

//Clears the screen
void clearScreen ()
{
        for (int i = 0; i <= 100; i++)
        {
                cout << '\n';
        }
}

//Prints the categoryStorage without sense of order
void SimplePrintCategoryStorage (vector<category> categoryStorage)
{
        for (int i = 0; i < categoryStorage.size (); i++)
        {
                cout << categoryStorage[i].categoryName << '\n';
        }
}

//Prints the categoryStorage with the full structure, without sense of order
void FullPrintCategoryStorage (vector<category> categoryStorage)
{
        for (int i = 0; i < categoryStorage.size (); i++)
        {
                cout << "categoryName: " << categoryStorage[i].categoryName << '\n';

                cout << "categoryPosition: " << categoryStorage[i].categoryPosition << '\n';

                cout << "categoryLayer: " << categoryStorage[i].categoryLayer << '\n';

                cout << "categoryChildren: ";
                for (int j = 0; j < categoryStorage[i].categoryChildren.size(); j++)
                {
                        cout << categoryStorage[i].categoryChildren[j] << ' ';
                }
                cout << '\n';

                cout << "categoryParent: "<< categoryStorage[i].categoryParent << '\n';

                cout << "categoryTags: ";
                for (int j = 0; j < categoryStorage[i].categoryTags.size(); j++)
                {
                        cout << categoryStorage[i].categoryTags[j] << ' ';
                }
                cout << '\n' << '\n';
        }
}

//Prints the categories names indented with tabs, with sense of order
void PrintCategoryStorage (vector<category> categoryStorage, vector<int> categoryChildren, int layer)
{
        for (int i = 0; i < categoryChildren.size (); i++)
        {
                for (int i = 0; i < layer; i++)
                {
                        cout << '\t';
                }
                cout << categoryStorage[categoryChildren[i]].categoryName << '\n';
                PrintCategoryStorage (categoryStorage, categoryStorage[categoryChildren[i]].categoryChildren, layer+1);
        }
}

//Checks if a category is in the categoryStorage
bool IsCategoryInCategoryStorage (vector<category> categoryStorage, string category)
{
        for (int i = 0; i < categoryStorage.size (); i++)
        {
                if (category == categoryStorage[i].categoryName)
                {
                        return true;
                }
        }
        return false;
}

//Find the position of a category by name
int CategoryPositionFinder (vector<category> categoryStorage, string category)
{
        if (IsCategoryInCategoryStorage (categoryStorage, category))
        {
                for (int i = 0; i < categoryStorage.size (); i++)
                {
                        if (category == categoryStorage[i].categoryName)
                        {
                                return i;
                        }
                }
                return -1;
        }
        else
        {
                return -1;
        }
}

//Returns the deepest layer
int HowDeepDeepestLayer (vector<category> categoryStorage)
{
        int deepestLayer = 0;
        for (int i = 0; i < categoryStorage.size (); i++)
        {
                if (categoryStorage[i].categoryLayer > deepestLayer)
                {
                        deepestLayer = categoryStorage[i].categoryLayer;
                }
        }
        return deepestLayer;
}

//Counts how many children have the same layer and parent as child
int HowManySameParentSameLayer (vector<category> categoryStorage, int child)
{
        int counter = 0;
        for (int i = 0; i < categoryStorage.size (); i++)
        {
                if
                (
                        categoryStorage[i].categoryLayer == categoryStorage[child].categoryLayer &&
                        categoryStorage[i].categoryParent == categoryStorage[child].categoryParent
                )
                {
                        counter++;
                }
        }
        return counter;
}

//A function that returns the position of a child in an array of children with the same layer and parent
int SameParentSameLayerPos (vector<category> categoryStorage, int child)
{
        vector<int> children;
        for (int i = 0; i < categoryStorage.size (); i++)
        {
                if
                (
                        categoryStorage[i].categoryLayer == categoryStorage[child].categoryLayer &&
                        categoryStorage[i].categoryParent == categoryStorage[child].categoryParent
                )
                {
                        children.push_back(i);
                }
        }
        for (int i = 0; i < children.size (); i++)
        {
                if (child == children[i])
                {
                        return i;
                }
        }
        return -1;
}

void PrintHeritage (int child, vector<int> heritage, vector<category> categoryStorage)
{
        int deepestLayer = HowDeepDeepestLayer (categoryStorage);

        if (categoryStorage[child].categoryParent == -1)
        {
                for (int i = heritage.size ()-1; i >= 0; i--)
                {
                        cout << '"';
                        for (int j = 0; j < categoryStorage[heritage[i]].categoryTags.size(); j++)
                        {
                                cout << categoryStorage[heritage[i]].categoryTags[j] << ' ';
                        }

                        for (int j = 0; j < deepestLayer - categoryStorage[heritage[i]].categoryTags.size(); j++)
                        {
                                cout << "0 ";
                        }
                        cout << categoryStorage[heritage[i]].categoryName << '"' << ' ';
                }
        }
        else
        {
                heritage.push_back (child);

                PrintHeritage (categoryStorage[child].categoryParent, heritage, categoryStorage);
        }
}

void PrintCategoryStorageWithTags (vector<category> categoryStorage, vector<int> categoryChildren, int layer)
{
        //Scrolls through the children
        for (int i = 0; i < categoryChildren.size (); i++)
        {
                //Prints out the tabs
                for (int j = 0; j < layer; j++)
                {
                        cout << '\t';
                }

                //Prints out the name of the category
                cout << categoryStorage[categoryChildren[i]].categoryName << ' ';

                //Print heritage here
                vector<int> heritage;
                PrintHeritage (categoryChildren[i], heritage, categoryStorage);
                cout << '\n';

                //Call the function for the children's children recursively
                PrintCategoryStorageWithTags (categoryStorage, categoryStorage[categoryChildren[i]].categoryChildren, layer+1);
        }
}

//The user interface
vector<category> userInterfaceCategoryStorage ()
{
        vector<category> categoryStorage;

        category root;
                root.categoryName = "root";
                root.categoryPosition = 0;
                root.categoryLayer = 0;
                vector<int> rootCategoryChildren = {};
                root.categoryChildren = rootCategoryChildren;
                root.categoryParent = -1;
                vector<int> rootCategoryTags = {};
                root.categoryTags = rootCategoryTags;
        categoryStorage.push_back (root);

        string userInputMenu;
        do
        {
                clearScreen ();

                cout << "Current category hierarchy:" << '\n' << '\n';

                PrintCategoryStorage (categoryStorage, categoryStorage[0].categoryChildren, 0);
                cout << '\n';
                cout << "----------------------------------------------" << '\n';
                cout << '\n';
                cout << "E) Enter a new (sub)category" << '\n';
                cout << "Q) Quit and print out the categories in the correct format" << '\n';

                cin >> userInputMenu;

                if (userInputMenu == "E")
                {
                        cout << '\n';
                        cout << "Enter the name of the (sub)category you want this (sub)category to fall under" << '\n';
                        cout << "If this is the first layer of categories, enter 'root' " << '\n';

                        string userInputCategoryToFallUnder;
                        cin >> userInputCategoryToFallUnder;

                        if (IsCategoryInCategoryStorage(categoryStorage, userInputCategoryToFallUnder))
                        {
                                cout << '\n';
                                cout << "Enter the name of the (sub)category you want to add" << '\n';

                                string userInputCategory;
                                cin >> userInputCategory;

                                int categoryToFallUnderPos = CategoryPositionFinder (categoryStorage, userInputCategoryToFallUnder);

                                categoryStorage[categoryToFallUnderPos].categoryChildren.push_back (categoryStorage.size ());

                                category emptyCategory;
                                        emptyCategory.categoryName = userInputCategory;
                                        emptyCategory.categoryPosition = categoryStorage.size ();
                                        emptyCategory.categoryLayer = categoryStorage[categoryToFallUnderPos].categoryLayer+1;
                                        emptyCategory.categoryChildren = {};
                                        emptyCategory.categoryParent = categoryToFallUnderPos;
                                        emptyCategory.categoryTags = categoryStorage[categoryToFallUnderPos].categoryTags;
                                categoryStorage.push_back(emptyCategory);
                                categoryStorage[categoryStorage.size ()-1].categoryTags.push_back (SameParentSameLayerPos (categoryStorage, categoryStorage.size ()-1)+1);
                        }
                        else
                        {
                                cout << "That is not a valid (sub)category" << '\n';
                        }
                }
                else if (userInputMenu == "Q")
                {
                        //Do nothing
                }
                else
                {
                        cout << "That is not one of the options" << '\n';
                }
        }
        while (userInputMenu!="Q");

        clearScreen ();

        return categoryStorage;

}

int main ()
{
        vector<category> categoryStorage = userInterfaceCategoryStorage ();

        cout << "Category hierarchy with tags, add the tags behind the RSS urls in Newsboat:" << '\n' << '\n';
        PrintCategoryStorageWithTags (categoryStorage, categoryStorage[0].categoryChildren, 0);
        //FullPrintCategoryStorage (categoryStorage);
}
