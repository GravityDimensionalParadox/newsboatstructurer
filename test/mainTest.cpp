#include "../src/main.cpp"

/*
struct category
{
        string categoryName;
        int categoryPosition;
        int categoryLayer;
        vector<int> categoryChildren;
        int categoryParent;
};
*/

/*
        0        root
        1                test1
        2                        test1_1
        3                        test1_2
        4                        test1_3
        5                test2
        6                        test2_1
        7                        test2_2
        8                        test2_3
        9                test3
        10                       test3_1
        11                       test3_2
        12                       test3_3
*/
vector<category> organizedCategoryStorage ()
{
        vector<category> categoryStorage;

        category root;
                root.categoryName = "root";
                root.categoryPosition = 0;
                root.categoryLayer = 0;
                vector<int> rootCategoryChildren = {1, 5, 9};
                root.categoryChildren = rootCategoryChildren;
                root.categoryParent = -1;
        categoryStorage.push_back (root);

                category test1;
                        test1.categoryName = "test1";
                        test1.categoryPosition = 1;
                        test1.categoryLayer = 1;
                        vector<int> test1CategoryChildren = {2, 3, 4};
                        test1.categoryChildren = test1CategoryChildren;
                        test1.categoryParent = 0;
                categoryStorage.push_back (test1);

                        category test1_1;
                                test1_1.categoryName = "test1_1";
                                test1_1.categoryPosition = 2;
                                test1_1.categoryLayer = 2;
                                vector<int> test1_1CategoryChildren = {};
                                test1_1.categoryChildren = test1_1CategoryChildren;
                                test1_1.categoryParent = 1;
                        categoryStorage.push_back (test1_1);

                        category test1_2;
                                test1_2.categoryName = "test1_2";
                                test1_2.categoryPosition = 3;
                                test1_2.categoryLayer = 2;
                                vector<int> test1_2CategoryChildren = {};
                                test1_2.categoryChildren = test1_2CategoryChildren;
                                test1_2.categoryParent = 1;
                        categoryStorage.push_back (test1_2);

                        category test1_3;
                                test1_3.categoryName = "test1_3";
                                test1_3.categoryPosition = 4;
                                test1_3.categoryLayer = 2;
                                vector<int> test1_3CategoryChildren = {};
                                test1_3.categoryChildren = test1_3CategoryChildren;
                                test1_3.categoryParent = 1;
                        categoryStorage.push_back (test1_3);

                category test2;
                        test2.categoryName = "test2";
                        test2.categoryPosition = 5;
                        test2.categoryLayer = 1;
                        vector<int> test2CategoryChildren = {6, 7, 8};
                        test2.categoryChildren = test2CategoryChildren;
                        test2.categoryParent = 0;
                categoryStorage.push_back (test2);

                        category test2_1;
                                test2_1.categoryName = "test2_1";
                                test2_1.categoryPosition = 6;
                                test2_1.categoryLayer = 2;
                                vector<int> test2_1CategoryChildren = {};
                                test2_1.categoryChildren = test2_1CategoryChildren;
                                test2_1.categoryParent = 5;
                        categoryStorage.push_back (test2_1);

                        category test2_2;
                                test2_2.categoryName = "test2_2";
                                test2_2.categoryPosition = 7;
                                test2_2.categoryLayer = 2;
                                vector<int> test2_2CategoryChildren = {};
                                test2_2.categoryChildren = test2_2CategoryChildren;
                                test2_2.categoryParent = 5;
                        categoryStorage.push_back (test2_2);

                        category test2_3;
                                test2_3.categoryName = "test2_3";
                                test2_3.categoryPosition = 8;
                                test2_3.categoryLayer = 2;
                                vector<int> test2_3CategoryChildren = {};
                                test2_3.categoryChildren = test2_3CategoryChildren;
                                test2_3.categoryParent = 5;
                        categoryStorage.push_back (test2_3);

                category test3;
                        test3.categoryName = "test3";
                        test3.categoryPosition = 9;
                        test3.categoryLayer = 1;
                        vector<int> test3CategoryChildren = {10, 11 ,12};
                        test3.categoryChildren = test3CategoryChildren;
                        test3.categoryParent = 0;
                categoryStorage.push_back (test3);

                        category test3_1;
                                test3_1.categoryName = "test3_1";
                                test3_1.categoryPosition = 10;
                                test3_1.categoryLayer = 2;
                                vector<int> test3_1CategoryChildren = {};
                                test3_1.categoryChildren = test3_1CategoryChildren;
                                test3_1.categoryParent = 9;
                        categoryStorage.push_back (test3_1);

                        category test3_2;
                                test3_2.categoryName = "test3_2";
                                test3_2.categoryPosition = 11;
                                test3_2.categoryLayer = 2;
                                vector<int> test3_2CategoryChildren = {};
                                test3_2.categoryChildren = test3_2CategoryChildren;
                                test3_2.categoryParent = 9;
                        categoryStorage.push_back (test3_2);

                        category test3_3;
                                test3_3.categoryName = "test3_3";
                                test3_3.categoryPosition = 12;
                                test3_3.categoryLayer = 2;
                                vector<int> test3_3CategoryChildren = {};
                                test3_3.categoryChildren = test3_3CategoryChildren;
                                test3_3.categoryParent = 9;
                        categoryStorage.push_back (test3_3);

        return categoryStorage;
}

/*
        0       root
        1                test3
        2                        test2_2
        3                test2
        4                        test1_1
        5                        test3_2
        6                        test3_3
        7                        test2_3
        8               test1
        9                       test2_1
        10                      test3_1
        11                      test1_2
        12                      test1_3
*/
vector<category> unorganizedCategoryStorage ()
{
        vector<category> categoryStorage;

        category root;
                root.categoryName = "root";
                root.categoryPosition = 0;
                root.categoryLayer = 0;
                vector<int> rootCategoryChildren = {8, 3, 1};
                root.categoryChildren = rootCategoryChildren;
                root.categoryParent = -1;
        categoryStorage.push_back (root);

                category test3;
                        test3.categoryName = "test3";
                        test3.categoryPosition = 1;
                        test3.categoryLayer = 1;
                        vector<int> test3CategoryChildren = {10, 5, 6};
                        test3.categoryChildren = test3CategoryChildren;
                        test3.categoryParent = 0;
                categoryStorage.push_back (test3);

                        category test2_2;
                                test2_2.categoryName = "test2_2";
                                test2_2.categoryPosition = 2;
                                test2_2.categoryLayer = 2;
                                vector<int> test2_2CategoryChildren = {};
                                test2_2.categoryChildren = test2_2CategoryChildren;
                                test2_2.categoryParent = 5;
                        categoryStorage.push_back (test2_2);

                category test2;
                        test2.categoryName = "test2";
                        test2.categoryPosition = 3;
                        test2.categoryLayer = 1;
                        vector<int> test2CategoryChildren = {9, 2, 7};
                        test2.categoryChildren = test2CategoryChildren;
                        test2.categoryParent = 0;
                categoryStorage.push_back (test2);

                        category test1_1;
                                test1_1.categoryName = "test1_1";
                                test1_1.categoryPosition = 4;
                                test1_1.categoryLayer = 2;
                                vector<int> test1_1CategoryChildren = {};
                                test1_1.categoryChildren = test1_1CategoryChildren;
                                test1_1.categoryParent = 1;
                        categoryStorage.push_back (test1_1);

                        category test3_2;
                                test3_2.categoryName = "test3_2";
                                test3_2.categoryPosition = 5;
                                test3_2.categoryLayer = 2;
                                vector<int> test3_2CategoryChildren = {};
                                test3_2.categoryChildren = test3_2CategoryChildren;
                                test3_2.categoryParent = 9;
                        categoryStorage.push_back (test3_2);

                        category test3_3;
                                test3_3.categoryName = "test3_3";
                                test3_3.categoryPosition = 6;
                                test3_3.categoryLayer = 2;
                                vector<int> test3_3CategoryChildren = {};
                                test3_3.categoryChildren = test3_3CategoryChildren;
                                test3_3.categoryParent = 9;
                        categoryStorage.push_back (test3_3);

                        category test2_3;
                                test2_3.categoryName = "test2_3";
                                test2_3.categoryPosition = 7;
                                test2_3.categoryLayer = 2;
                                vector<int> test2_3CategoryChildren = {};
                                test2_3.categoryChildren = test2_3CategoryChildren;
                                test2_3.categoryParent = 5;
                        categoryStorage.push_back (test2_3);

                category test1;
                        test1.categoryName = "test1";
                        test1.categoryPosition = 8;
                        test1.categoryLayer = 1;
                        vector<int> test1CategoryChildren = {4, 11, 12};
                        test1.categoryChildren = test1CategoryChildren;
                        test1.categoryParent = 0;
                categoryStorage.push_back (test1);

                        category test2_1;
                                test2_1.categoryName = "test2_1";
                                test2_1.categoryPosition = 9;
                                test2_1.categoryLayer = 2;
                                vector<int> test2_1CategoryChildren = {};
                                test2_1.categoryChildren = test2_1CategoryChildren;
                                test2_1.categoryParent = 5;
                        categoryStorage.push_back (test2_1);

                        category test3_1;
                                test3_1.categoryName = "test3_1";
                                test3_1.categoryPosition = 10;
                                test3_1.categoryLayer = 2;
                                vector<int> test3_1CategoryChildren = {};
                                test3_1.categoryChildren = test3_1CategoryChildren;
                                test3_1.categoryParent = 9;
                        categoryStorage.push_back (test3_1);

                        category test1_2;
                                test1_2.categoryName = "test1_2";
                                test1_2.categoryPosition = 11;
                                test1_2.categoryLayer = 2;
                                vector<int> test1_2CategoryChildren = {};
                                test1_2.categoryChildren = test1_2CategoryChildren;
                                test1_2.categoryParent = 1;
                        categoryStorage.push_back (test1_2);

                        category test1_3;
                                test1_3.categoryName = "test1_3";
                                test1_3.categoryPosition = 12;
                                test1_3.categoryLayer = 2;
                                vector<int> test1_3CategoryChildren = {};
                                test1_3.categoryChildren = test1_3CategoryChildren;
                                test1_3.categoryParent = 1;
                        categoryStorage.push_back (test1_3);

        return categoryStorage;
}

void SimplePrintCategoryStorage ()
{
        vector<category> categoryStorage0 = organizedCategoryStorage ();
        SimplePrintCategoryStorage (categoryStorage0);

        cout << '\n';

        vector<category> categoryStorage1 = unorganizedCategoryStorage ();
        SimplePrintCategoryStorage (categoryStorage1);
}

void PrintCategoryStorageTest ()
{
        vector<category> categoryStorage0 = organizedCategoryStorage ();
        PrintCategoryStorage (categoryStorage0, categoryStorage0[0].categoryChildren, 0);

        cout << '\n';

        vector<category> categoryStorage1 = unorganizedCategoryStorage ();
        PrintCategoryStorage (categoryStorage1, categoryStorage1[0].categoryChildren, 0);
}

bool IsCategoryInCategoryStorageTest ()
{

}

bool CategoryPositionFinder ()
{

}

void userInterfaceCategoryStorageTest ()
{
        vector<category> categoryStorage = userInterfaceCategoryStorage ();
        //PrintCategoryStorage (categoryStorage, categoryStorage[0].categoryChildren, 0);
}

int main ()
{
        //SimplePrintCategoryStorage (); PASSED
        //PrintCategoryStorageTest (); PASSED
        userInterfaceCategoryStorageTest ();
}
